---
title: Practical TLS Advice
paginate: true
theme: gaia
auto-scaling: true
style: |
  footer { font-size: 12pt; }
  section {
    background-color: #bfd3cd;
  }
footer: https://gitlab.com/markphahn/practical-tls-advice
---

<style>
.abc { font-size: 14px; }
ul.abc { font-size: 14px; }
li.abc { font-size: 14px; }
</style>

# Practical TLS Advice <br>*for 2021-2030*

![Ted Hahn](https://www.tcbtech.com/wp-content/uploads/2020/11/ted.jpg) Ted Hahn, TCB Technologies, Inc.

<!-- Ted Hahn is an SRE for hire working on planet-scale distributed systems. His clients include Epic Games and startups in Seattle and New York. -->

![Mark Hahn](https://www.tcbtech.com/wp-content/uploads/2021/09/Mark_Casual_Head_GreenBG.png) Mark Hahn, Ciber Global

<!-- Mark Hahn is Director of Cloud Strategies and DevOps for Ciber Global, a consulting firm. In this role he is responsible for all things related to software velocity. -->

<!-- we are father son in the same space and we like giving talks together. -->

---
# Motivation:

## You want to run a secure application. 
What does that mean?
- **Encryption-in-transit**
- End-to-end

<!-- 
Mark: We use TLS. It's the *only* standard for encrypting traffic on the web.

Ted: We’re not covering end-to-end encryption. We're talking about Applications and APIs that need secure communication between every pair of communicating components, sometimes called "Encrypted-In-Transit". End-to-end encryption is for clients communicating with each other. We're talking about securing your infrastructure internally, between server components that trust each other, and assuring that they can trust each other.

-->

---
# Part 1: Basics
<!-- Mark: -->
---
<style scoped>
  ul {
    margin-top: 0px;
    margin-bottom: 0px;
	font-size: 24pt;
  }
  li {
    margin-top: 0px;
    margin-bottom: 0px;
 }
 p {
    margin-top: 0px;
    margin-bottom: 0px;
 }
</style>

# The Public Trust domain
<!-- 


Ted: What does that certificate actually say? We sometimes summarize it as "The website is trustworthy", but that's not actually a judgement the computer can make. IT's also referred to as "they are who they say they are", but you don't really have that, either. What certificates can prove is that an *authority* says who you they are, and you implicitly trust the authority to be correct. Those Authorities, 
-->

<!--
Mark: Browser vendors and Operating System providers maintain lists of trusted Public CAs. They're not 100% the same, but the [browser forum](https://cabforum.org/) has laid down guidelines, so in practice the lists are at most times mostly the same. 

-->
**Public CA Lists : CAs, Intermediates, and Certificates**

- [Google](https://www.chromium.org/Home/chromium-security/root-ca-policy)
  - Chrome, Android, "Distroless" docker images
- [Mozilla](https://wiki.mozilla.org/CA)
  - Ubuntu, FreeBSD
- [Apple](https://support.apple.com/en-bh/HT212140)
  - iOS, MacOS
- [Microsoft](https://docs.microsoft.com/en-us/security/trusted-root/participants-list)
  - Windows

<!--
Mark: See the appendix for their advice on certificate management and how to implement TLS 
-->
---
<style scoped>
p,ul,li,span {
  font-size: 16pt;
}
</style>

<!-- 

Mark:
You've seen this before from `openssl x509`

# What’s in a certificate
  - SNI, CN, usages, who cares what else
  - Constraints
  - SubjectKeyId and AuthorityKeyID
  - Technical constraints on key, etc.
* Version 1 - 1988
* Version 2 - (unk)
* Version 3 - 1999 (rfc2459, now rfc5280)

X.509 Version 3 is the most recent (1999) and supports the notion of
extensions, whereby anyone can define an extension and include it in
the certificate. Some common extensions in use today are: 
* KeyUsage (limits the use of the keys to particular purposes such as "signing-only") 
* AlternativeNames (allows other identities to also be associated with this public key, e.g. DNS names, Email addresses, IP addresses

See: https://www.ibm.com/docs/en/sdk-java-technology/7?topic=certificate-x509-certificates
-->
```
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            04:69:93:b5:5d:d4:9e:82:4d:99:ae:15:a1:4e:30:cc:9e:a9
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C=US, O=Let's Encrypt, CN=R3
        Validity
            Not Before: Sep 16 02:58:20 2021 GMT
            Not After : Dec 15 02:58:19 2021 GMT
        Subject: CN=demo1.do.tcbtech-corp.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:ac:de:6e:66:de:29:b1:e4:23:de:f7:52:34:b7:
```

---
<style scoped>
p,ul,li,span {
  font-size: 14pt;
}
</style>

<!-- 
Mark:

- Version 3 - 1999 (rfc2459, now rfc5280)
- Wikipedia https://en.wikipedia.org/wiki/X.509#Structure_of_a_certificate
- Serial is a tracking id by the Issuer 
- Signature Algorithm: crypto choice for signature
-->

### X.509 version 3 structure
```
    Certificate
        Version Number
        Serial Number
        Signature Algorithm ID
        Issuer Name
        Validity period (Not Before, Not After)
        Subject name
        Subject Public Key Info (Public Key Algorithm, Subject Public Key)
        Issuer Unique Identifier (optional)
        Subject Unique Identifier (optional)
        Extensions (optional)
            ...
    Certificate Signature Algorithm
    Certificate Signature
```	
---
<style scoped>
pre {
  margin-top: 16px;
}
p,ul,li,span {
  font-size: 12pt;
}
</style>

<!-- 
Ted:
- Most of the magic of x509 certificates is in the certificate extensions

- Subject Alternative name: the list of DNS names for which this certificate is valid
- Key usage is older in the spec, Extended key usage is in newer specs
- Basic Constraints: CA or not
- Subject Key Identifier: provides a means of identifying certificates that contain a particular public key.
  - in a CA the SKI must be derived from the public key, e.g. 160-bit SHA-1 hash
  - https://datatracker.ietf.org/doc/html/rfc3280#section-4.2.1.2
- Authority Key Identifier: used to chain to a cert 
  - https://datatracker.ietf.org/doc/html/rfc5280#section-6
Mark:
- X509 Certificates are chains. There’s a set of public certificates that are “root CAs” for browsers. These are a trust domain. You also want to connect to other internal infrastructure services. These are another trust domain. We’re only briefly going to discuss how to connect to third-party services. Typically, you’ll be connecting to them via public endpoints.
- Authority Infortmation Access
  - OCSP access method
  - CA's policy information
- X509v3 Certificate Policies: formal specification for the valid uses of this certificate


-->

### X.509 version 3 extensions
```
X509v3 Subject Alternative Name: 
    DNS:demo1.do.tcbtech-corp.com, DNS:demo2.do.tcbtech-corp.com
X509v3 Key Usage: critical
    Digital Signature, Key Encipherment
X509v3 Extended Key Usage: 
    TLS Web Server Authentication, TLS Web Client Authentication
X509v3 Basic Constraints: critical
    CA:FALSE
X509v3 Subject Key Identifier: 
    25:DD:9F:3F:0D:C4:65:EA:5F:FF:5D:69:E7:F6:75:03:46:B3:C2:7F
X509v3 Authority Key Identifier: 
    keyid:14:2E:B3:17:B7:58:56:CB:AE:50:09:40:E6:1F:AF:9D:8B:14:C2:C6
Authority Information Access: 
    OCSP - URI:http://r3.o.lencr.org
    CA Issuers - URI:http://r3.i.lencr.org/
X509v3 Certificate Policies: 
    Policy: 2.23.140.1.2.1
    Policy: 1.3.6.1.4.1.44947.1.1.1
      CPS: http://cps.letsencrypt.org
```	
---

<!-- 
Mark: TLS 1.2 is the standard now - PCI Requires it. [TLS 1.3](https://datatracker.ietf.org/doc/html/rfc8446) was published in 2018. Last browser that didn’t support tls1.2 was in 2014. Every major browser has disabled TLS1.1 and below as of 2021. You should use TLS1.3. It is faster, more secure, and simpler.

Picture credit to (inspiration from):
A10 Networks Blog, Babur Khan, August 3, 2020 
https://www.a10networks.com/blog/key-differences-between-tls-1-2-and-tls-1-3/

Diagrams in https://www.websequencediagrams.com/

title TLS 1.2

Client->Server: 1. Hello, SNI
Server->Client: 2. Hello, Certificate
Client->Server: 3. Key Exchange, Change cipher spec
Server->Client: 4. Change cipher spec
Client->Server: 5. HTTP Request
Server->Client: 6. HTTP Response

title TLS 1.3

Client->Server: 1. Hello, SNI, Key Share
Server->Client: 2. Hello, Key Share, Certificate, Verify
Client->Server: 3. HTTP Request
Server->Client: 4. HTTP Response

-->

#### Technical Details about TLS
![height:500](diagrams/tls_1.2_1.3.png)

---
<style scoped>
p,ul,li,span {
  font-size: 20pt;
}
</style>

<!--
Ted: One of the biggest improvements in TLS 1.3 is a simplified set of Cipher suites. They are simple, they are secure, and predicted to remain so
If you're using TLS 1.2, see See the go.dev link for good explanation of which to use, or the Mozilla Configurator.
-->

# Cipher suites in TLS 1.3.
- TLS_AES_128_GCM_SHA256
- TLS_AES_256_GCM_SHA384
- TLS_CHACHA20_POLY1305_SHA256
- TLS_AES_128_CCM_SHA256
- TLS_AES_128_CCM_8_SHA256

versus 36 choices in TLS 1.2

- https://go.dev/blog/tls-cipher-suites 

<!--
- tls1.2
  - https://datatracker.ietf.org/doc/html/rfc5246#appendix-C
- tls1.3
  - https://datatracker.ietf.org/doc/html/rfc8446#appendix-B.4

- https://ssl-config.mozilla.org/
- https://en.wikipedia.org/wiki/Transport_Layer_Security#Cipher
-->

---
# Part 2: Trust on the Web
---
  <!-- Ted: -->
# Trust on the web
- [Certificate Transparency](https://datatracker.ietf.org/doc/html/rfc6962)
- Use short lived certificates (90 days)
- Use [HSTS](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
  <!-- HSTS, or HTTP Strict Transport Security, is a header you can add to your website to inform browsers that they should only connect over HTTPS/TLS. You sites should all be available by HTTPS primarially, and there's little reason not to add this header. -->
- Use cookie management
  <!-- Cookies should be set with the `Secure;` attribute. Additionally, you probably want to be careful with the `SameSite` and `Domain` attributes. -->
- Use separate domains

---

# How to get certificates <br>into applications
- Idea 1: Load Balancer in front. Rely on cloud provider
- Idea 2: ACME Certificate

<!--
Mark:
- If you use the certificate capabilities of your cloud provider they make getting public certificates easy and free
- ACME allows the use of [several free or low cost certificate providers](https://en.wikipedia.org/wiki/Automated_Certificate_Management_Environment#ACME_Service_Providers) (Let's Encrypt is the most famous)
-->

---
### Load Balancer profiles to use
<!-- This slide is purely practical: The TLS profiles to use with your Cloud Provider's load balancer. If you're running your own NGINX you're on your own, but this is a practical reference. -->
- Use the [Mozilla Configurator](https://ssl-config.mozilla.org/)
- AWS: ELBSecurityPolicy-FS-1-2-Res-2019-08
  <!-- 
  Policy Overview: https://docs.aws.amazon.com/elasticloadbalancing/latest/application/create-https-listener.html#fs-supported-policies
  Use one of the policies from the FS table.
  We suggest ELBSecurityPolicy-FS-1-2-Res-2019-08 as it is recommended by Mozilla. It supports only 1.2 and modern ciphers while still having early 1.2 compatibility.
  -->
- GCP: MODERN profile.
  <!--
  Policy Overview: https://cloud.google.com/load-balancing/docs/ssl-policies-concepts
  GCP is simplified in it's approach to profiles, and it's MODERN profile is compatible and follows a rolling best practice. Unlike AWS, you don't have to manually update it; Setting "MODERN" will follow changes to their recommendations.
  -->
- Azure: AppGwSslPolicy20170401S
  <!--
  Policy Overview: https://docs.microsoft.com/en-us/azure/application-gateway/application-gateway-ssl-policy-overview
  This is the only policy that does not support TLS 1.1 and below. 
  -->

---
<style scoped>
ul,li {
  font-size: 20pt;
  margin-top: 0px;
  margin-bottom: 0px;
}
p {
  margin-top: 0px;
  margin-bottom: 0px;
}
</style>

<!-- 
Mark:
- This isn't strictly TLS advice, but here's an example of how to organize your online presence.
- Possible URL organization where corporate and external urls are separated. 
- Use a different zone of trust for the corporate and external urls. We're not talking about a TLS zone of trust here, but cookie zones of trust. The browser maintains a cookie pool for each domain name, though the actual rules are a little bit more complicated. Again, see the developer documentation for cookies.
-->

## Use separate domains: <br>URL Organization

- External 
  - `www.example.com/`
  - `www.example.com/api1`
  - `www.example.com/api2`
- Corporate
  - `example-corp.com/app1`
  - `example-corp.com/app1/api1`
  - `example-corp.com/app2`
  - `example-corp.com/app2/api1`

---
<style scoped>
ul {
  font-size: 16pt;
  margin-top: 0px;
  margin-bottom: 0px;
}
li {
  font-size: 16pt;
  margin-top: 0px;
  margin-bottom: 0px;
}
</style>

## Use separate domains: <br>More mature URL Organization
<!-- Ted: -->
- Marketing
  - `www.example.com/`
- External
  - `app.example.com/`
  - `login.app.example.com/`
  - `api.app.example.com/api1`
  - `api2.app.example.com/`
  <!-- Using multiple subdomains, but rooting them in a common subdomain tree, allows you to share some cookies across your subdomains - Without leaking them to your marketing site. In this case, you can set common cookies, such as a shared session identifier, with the attribute Domain=app.example.com -->
- Corporate
  - `sso.example-corp.com`
  - `app1.example-corp.com`
  - `app2.example-corp.com/app2`
  - `example-corp.com/app2/api1`
  <!-- Your corporate apps don't have a marketing site, so you can share a domain for them, but most likely it will be more convienient to completely separate them. Again, keeping the cookies defined per-site and site-per-application prevents information leakage between applications. -->

---
# Revocation of trust
<!-- 
Ted: 
Certificates come with expiry times, the assertions in them don't hold forever. However, sometimes expiry isn't enough, you need to revoke trust from a certificate early. There are a couple methods to do this:
-->
  - Certificate Transparency
  <!-- Mark: Certificate Transparency is a relative newcomer, and is not a revocation method in and of itself. It is, however, a way of making revocation much easier by making the list of issued certificate publically available and auditable - It allows security researchers to probe and question, and SRE teams to notice if someone else has been issued a certificate for the sites under their care -->
  - CRLs - Certificate Revocation Lists
  <!-- Certificate Revocation Lists, or CRLs, are simply that: A list of certificates that have been revoked. They are a blacklisting approach to revocation. When private keys are leaked or exposed, or when certificates are issued to parties that they shouldn't have been, they can be blacklisted.-->
  - OCSP - Online Certificate Status Protocol
  <!-- Online Certificate Status Protocol, or OCSP, is a method for checking if a certificate has been revoked in real-time. It is a flawed design, because it effectively reduces the connection process to a three-way-handshake. A follow up, OCSP Stapling, attempted to resolve this flaw by "stapling" short-lived OCSP responses to certificate handshakes. In this way, OCSP Stapling was effectively simply replacing the validity period on the certificate. As of now[1], of the major browsers, only Edge in Enterprise configurations considers OCSP a hard-fail. -->

<!--
Ted: 
CRLs have the problem of being slow-moving and relatively expensive, client-side, as every client needs a complete CRL list. On the other hand, they only need to contain certificates that have not yet expired, and you can use CRLs from multiple sources - Taking the responsibility (and control) of revocation from the CA vendors that may have been the initial source of error.
-->

<!--
The TLS Certificate Status Request extension is specified in RFC 6066, Section 8. 
- https://datatracker.ietf.org/doc/html/rfc6066#section-8
- https://datatracker.ietf.org/doc/html/rfc2560

[1] See SSL.com's review of Revocation of trust: https://www.ssl.com/blogs/how-do-browsers-handle-revoked-ssl-tls-certificates/
-->

---
# Part 2: in summary
- What is a **Trust Domain**
  - Certificates, TLS, OCSP, et. al. are the building blocks of trust domains
  - A CA
  - Intermediates
    - https://letsencrypt.org/certificates/
  - Claims

<!--
Ted:
We've talked about Trust on the web. This is one Trust domain. We're introducing this security term, which is an abstract concept.
A Certificate Authority uses Certificates, TSL, Cert Transparency etc. to build a Trust Domain in the public sphere. 
Each certificate makes some "Claims".
The intermediate certificates build chains from individual certificates to roots.
Intermediate signing (certificate) authorities allows root CAs to be off-line, but still allow for automated certificate creation. 
This increases the strength of the root ca. 

Web server certificates (and SMTP certificates as well) provide a set of claims that are asserted by the CA's digital signature (or intermediate's digital signature).

This allows the major (and minor) browsers to trust that content is properly encrypted and originating from a defined source. (For some suitable definition of "defined".)

-->


---

# Part 3: Local Trust Domains
<!-- Mark: -->
---

# Zones of Trust 

![width:1122x height:422px](diagrams/Zones_of_Trust.svg)

<!--
Basic diagram of what a split of a zone of trust might look like.
-->

<!--
Mark:
Here's an example of how to lay out your infrastructure architecture. Your frontend load balancer should present a certificate from the Public zone of trust to your clients. That load balancer should provide the bastion over the boundary between the public and private zones of trust.
-->

---

# Zones of Trust 

![width:1122x height:422px](diagrams/Zones_of_Trust_with_Cluster.svg)

<!--
Ted:
Basic diagram of what a split of a zone of trust where the Kubernetes Cluster provides the local zone of trust.
-->

<!--
You should run your own CA, defining a Zone of Trust. You should probably run several - One for each Environment you plan to run. Create a CA for development, staging, and production. Applications in each of these zones should generally not talk to each other.

Having a Trust domain within your infrastructure is a building block. You still need to implement proper role-based-access-control and authentication behind it, but using TLS internally assures that even an attacker who can sniff and inject directly in your network cannot compromise you.
-->
---

# How to run a local CA
- Cert Manager
- Let's Encrypt Boulder
- Cloudflare CFSSL
- Istio
- Vault
- Cloud HSM tools

<!--
Mark:
- We will talk about methods for using each for using each of these
- But first let's talk about a *private trust model*
- While you can refine it incrementally, you need a starting point and goal
-->

---
<style scoped>
ul, li {
  font-size: 24pt;
  margin-top: 0px;
  margin-bottom: 0px;
}
</style>

## Trust Model for your Trust Domain

Model : SNI and trust your Private CA cert
  - This only applies to infrastructure applications
  - Business users use public certificates on the `corp` domain
  - Create a CA bundle with your private CA and inject it into your infrastructure applications
  - Use short certificate lifetimes
  - Don't add your private CA Cert to your public roots of trust
  - Add Revocation and Transparency later

<!--
Ted:
- You can get away with 30 day certificate lifetimes if you assured to be restarting your applications on a regular basis.
Mark:
- This only applies to internal infrastructure applications;
  - anything that uses a public browser should use a public certificate
  - (terminated at your front end load-balancer)
- Note that business users should use public browsers for corporate applications using your `corp` domain.
-->
---

<style scoped>
ul, li {
  font-size: 24pt;
  margin-top: 0px;
  margin-bottom: 0px;
}
</style>

## How to run a local CA
<!-- Mark: -->
- [Cert Manager](https://cert-manager.io/)
  <!-- Cert Manager is a tool for Kubernetes that automates generating certificates. It adds to Kubernetes object types representing Certificates and "Issuers", the latter of which are connections to CAs. -->
  - Run as an internal CA
    <!-- Cert Manager has an issuer type called "CA". The CA issuer uses a CA keypair in a secret on the cluster to issue certificates. Anyone with the appropriate RBAC access can create a certificate object and have it signed by Cert Manager's CA. This is a very simple solution, but it protects against simple packet-sniffing attacks as well as unsophisticated MITM attacks. -->
  - Run as front end to ACME, e.g. Let’s Encrypt
    <!-- One solution to local certificate issuance is to run a copy of Let's Encrypt's Boulder software.
	-->
- [Let's Encrypt Boulder](https://github.com/letsencrypt/boulder)
  - Boulder is the software that runs Let's Encrypt.
  - Supports everything Let's Encrypt does

<!-- Full copy of the software that run's Let's Encrypt -->

---

<style scoped>
ul, li {
  font-size: 24pt;
  margin-top: 0px;
  margin-bottom: 0px;
}
</style>

## How to run a local CA

- [Vault](https://learn.hashicorp.com/tutorials/vault/pki-engine#policy-requirements) 
  - Vault is a popular choice from Hashicorp
  - Vault can be used as a backend to Certificate Manager

<!-- 
Ted:
Vault is a popular choice from Hashicorp. Vault can be used as a backend to Certificate Manager, or accessed through it's own own agent (possibly as a sidecar to your pods). 
-->

- [Cloudflare CFSSL](https://github.com/cloudflare/cfssl)
  - CFSSL implements a signing server, allowing you to build your own CA
  - CFSSL it maintains a certificate transparency log
  <!-- CFSSL Implements a signing server, allowing you to build your own CA. CFSSL is nice in that it maintains a certificate transparency log and can run as an OCSP server -->

---

<style scoped>
ul, li {
  font-size: 24pt;
  margin-top: 0px;
  margin-bottom: 0px;
}
</style>

## How to run a local CA
<!-- Mark -->
- [Istio](https://istio.io/latest/docs/tasks/security/cert-management/)
  - Istio runs a CA Issuer, simplifying deployment
  - It also provides proxies for your traffic
  <!-- The advantage of this is that it's transparent; The disadvantage is that it is quite all-in-one and opaque. -->

- Cloud HSM tools
  - Run on dedicated hardware
<!-- Cloud HSMs are a fantastic tool, because they take the management of the CA's private key out of your hands. Often they are run on dedicated hardware that has a limited number of crypto operations per second, limiting the number of certificate an attacker that compromises your infrastructure can issue. This dedicated hardware also has a lower attack surface. The downside of these appliances is that they're expensive, and both the previous upsides can be downsides - Should you need to issue a large number of certificates, or should the hardware have a vulnerability. 

- https://aws.amazon.com/cloudhsm/
- https://azure.microsoft.com/en-us/services/azure-dedicated-hsm
- https://cloud.google.com/kms/docs/hsm
-->

---

<style scoped>
ul, li {
  font-size: 24pt;
  margin-top: 0px;
  margin-bottom: 0px;
}
</style>

# How to get certificates <br>into applications, *reprise*

<!-- Mark: -->
<!-- Your application server should accept a TLS Server Certificate and Key locations as configuration parameters, and your client should have a parmeter for CA Bundle location. Your HTTP Client library has a default configuration, and you should not override that with your CA Bundle. Instead, in your client application, create a second HTTP client, and configure it with the CA Bundle. If you're going all the way to Mutual TLS Authentication, your configuration will look like a combination of the two; Certificate, Key, and CA Bundle on each side.
Popular applications like Postgres are routinely configured with TLS, but also support MTLS, either as a requirement for a connection, or going further into using it as an authentication method. This actually simplifies your configuration - Rather than dealing with passwords and rotating passwords, you simply authenticate as your application's principal, and certificate rotation serves to keep you secure over time.
 -->
- Put your Private Key, Certificate, and CA Bundle in a single clear folder
<!-- Ted: -->
<!-- A CA Bundle is very easy to generate. Most applications accept the standard CA format - A list of PEM-wrapped DER encoded certificates. -->
  - Generate certificates that include both server and client authentication
  <!-- X509v3 Extended Key Usage -->
- Use Kubernetes Secrets to provide certificates and and private keys to applications
  <!-- or Vault or other secrets manager -->
<!-- Mark: -->
- Use automation to push certificates to secure locations on your legacy style infrastructure

See `examples` folder in https://gitlab.com/gauntletwizard_net/kubetls

---

# Example

![width:1122x height:422px](diagrams/Certificate_Distribution.svg)

---
# Monitor your certificates.
<!-- 
Ted:
Now let's talk about a subject near and dear to our hearts: Monitoring. TLS fails very suddenly, by design, when certificates expire.
Prometheus' Blackbox monitor is drop-dead simple to set up. It exports information about your TLS certificate such as the expiry time. This makes it very easy to alert on certificates that are "about" to expire - With a proper ACME setup, you should be rotating at about 1/3rd of your validity period, and alert a couple days after that. With a default ACME setup, that gives you warning 2-3 weeks in advance. -->

```
# TYPE probe_ssl_earliest_cert_expiry gauge
probe_ssl_earliest_cert_expiry 1.637018287e+09
# HELP probe_ssl_last_chain_expiry_timestamp_seconds Returns last SSL chain expiry in timestamp seconds
# TYPE probe_ssl_last_chain_expiry_timestamp_seconds gauge
probe_ssl_last_chain_expiry_timestamp_seconds 1.637018287e+09
```
---

# Remote trust domains
<!-- You should communicate via authenticated public APIs. You rarely need to communicate with remote parties by private trust. -->
- Communicate with Public Endpoints
- Create a Trust domain for your external connections

---

# Thank you

- Ted Hahn, TCB Technologies, Inc.
  - THahn@TCBTech.com
- Mark Hahn, Ciber Global
  - mhahn@ciber.com

---

# Appendix: Relevant RFCs:
- TLS 1.3 - [RFC 8446](https://datatracker.ietf.org/doc/html/rfc8446)
- TLS 1.2 - [RFC 5246](https://datatracker.ietf.org/doc/html/rfc5246)
- x509v3 - [RFC 5280](https://datatracker.ietf.org/doc/html/rfc5280)
  - https://en.wikipedia.org/wiki/X.509
- OCSP - [RFC 2560](https://datatracker.ietf.org/doc/html/rfc2560)

---
# Vendors also have their own sets of TLS advice
- [Google](https://www.chromium.org/Home/chromium-security/education/tls#TOC-TLS-in-Chrome)
- [Mozilla](https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_Ciphersuite)
- [Apple](https://support.apple.com/guide/security/tls-security-sec100a75d12/web)
- [Microsoft](https://docs.microsoft.com/en-us/mem/configmgr/core/plan-design/security/enable-tls-1-2-server) has many technical documents.
  <!-- https://docs.microsoft.com/en-us/mem/configmgr/core/plan-design/security/enable-tls-1-2-server
    https://docs.microsoft.com/en-us/windows-server/security/tls/tls-ssl-schannel-ssp-overview -->
