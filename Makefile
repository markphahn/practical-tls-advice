presentation.html: presentation.md
	npx @marp-team/marp-cli presentation.md

presentation.pdf: presentation.md
	npx @marp-team/marp-cli presentation.md -o presentation.pdf

server:
	npx @marp-team/marp-cli@latest -s ./
