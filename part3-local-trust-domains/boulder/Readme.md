# Create a vritual machine

Use the azure user ssh key stored in Azure.

```
ssh -i ~/.ssh/tls-azure-user.pem azureuser@tls-boulder.westus2.cloudapp.azure.com

sudo apt -y install net-tools
sudo apt -y install golang
sudo apt -y install docker
sudo apt  install docker-compose

systemd docker start
sudo chmod 666 /var/run/docker.sock


curl http://localhost:4001/directory
curl --insecure https://localhost:4431/directory

curl https://boulder:4431/directory --resolve boulder:4431:127.0.0.1 --cacert tls_boulder.crt
```

openssl s_connect https://localhost:4431/directory


https://github.com/letsencrypt/boulder



