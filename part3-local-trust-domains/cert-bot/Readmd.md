# Overview

https://certbot-dns-linode.readthedocs.io/en/stable/

```
sudo certbot certificates
```


```
# Linode API credentials used by Certbot
dns_linode_key = 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ64
dns_linode_version = [<blank>|3|4]
```

```
sudo mkdir /var/cert-bot
touch /var/cert-bot/linode.ini
chmod 600 /var/cert-bot/linode.ini
sudo vi /var/cert-bot/linode.ini
```

Add in the key above

```
sudo certbot certonly \
  --dns-linode \
  --dns-linode-credentials /var/cert-bot/linode.ini \
  -d tls-test.tcbtech-corp.com
```
  
  
```
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Enter email address (used for urgent renewal and security notices)
 (Enter 'c' to cancel): mhahn@tcbtech.com

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf. You must
agree in order to register with the ACME server. Do you agree?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: y

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Would you be willing, once your first certificate is successfully issued, to
share your email address with the Electronic Frontier Foundation, a founding
partner of the Let's Encrypt project and the non-profit organization that
develops Certbot? We'd like to send you email about our work encrypting the web,
EFF news, campaigns, and ways to support digital freedom.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: y
Account registered.
Requesting a certificate for tcbtech-corp.com
Unsafe permissions on credentials configuration file: /var/cert-bot/linode.ini
Waiting 120 seconds for DNS changes to propagate

Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/tcbtech-corp.com/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/tcbtech-corp.com/privkey.pem
This certificate expires on 2021-12-12.
These files will be updated when the certificate renews.
Certbot has set up a scheduled task to automatically renew this certificate in the background.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
If you like Certbot, please consider supporting our work by:
 * Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
 * Donating to EFF:                    https://eff.org/donate-le
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

Edit the `/etc/nginx/sites-available/tls-test.tcbtech-corp.com` file to
use the right certificate path. 

```
sudo systemctl restart nginx
```


  
