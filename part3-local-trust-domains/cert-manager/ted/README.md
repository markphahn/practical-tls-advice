# Practical TLS on Kubernetes with cert-manager
In this article, we'll be exploring how to install cert-manager onto your kubernetes cluster, and how to use it to create certificates for your services as both the public trust domain and a private trust domain. For this, I'm assuming familiarity with the security model of a public and private trust domain. It's an overview, and not diving deep into the technical detail, however, this should be sufficient recipe for you to generate certificates.

## Part 0: Prep
First and foremost, [install cert-manager](https://cert-manager.io/docs/installation/). Their default installation is enough for this tutorial, though obviously read the documentation before using this in production:
    kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.5.4/cert-manager.yaml

This tutorial uses ClusterIssuers from cert-manager; These are non-namespaced resources. This effectively means that anyone on your cluster who has access to create `cert-manager.io/v1`/`certificaterequests` resources can issue certificates - Either public certificates under your domain, or private certificates (with any name and hostnames they so desire). This is reasonable - Even in production, you can keep that list of users small, and automate the process of creating certificates for your services.

## Part 1: Certificates on the public trust domain.
For this example, we'll be generating 

## Part 2: Certificates in your Private trust domain.
The first step to running a private CA is to generate the CA itself. If you already know how to do this, or have an intermediate certificate you wish to use, pick up from the step of creating the issuer secret. Alternatively, cert-manager has instructions for generating the CA through a [self-signed](https://cert-manager.io/docs/configuration/selfsigned/#bootstrapping-ca-issuers) issuer.

We'll create the private key and self-signed CA certificate using cfssl. We'll then upload the created files into a kubernetes as a secret. For clusterissuers, cert-manager requires that keys be in the namespace that cert-manager is running in (i.e. the `cert-manager` namespace in their default install)
    cd private/
    cfssl genkey -initca example-ca.csr.json | cfssljson -bare ca
    kubectl -n cert-manager create secret tls --key ca-key.pem --cert ca.pem local-ca

Once we've created this ClusterIssuer, we can use it to issue ourselves a certificate. This example certificate is created in a secret called `cert-manager-example-local`, and contains an example DNS name - that would match a service named `example`.
    kubectl apply -f local-signed-certificate.yaml
