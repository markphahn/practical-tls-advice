# Overview

*Note:* all demo work here uses the `cert-demo` namespace (or should).

*Note:* Make sure you get the domains right. In my case Lindoe is resposnible for `tcbtech-corp.com`
while Digital Ocean is responsable for `do.tcbtech-corp.com`. So for this demo all the dns names (and
common names, if used) are in the `do.tcbtech-corp.com` domain.

Create an api token in your DNS provider

```
echo -n <the token> | base64 > access-token
kubectl -n cert-demo create secret generic digitalocean-dns --from-file=./access-token
kubectl apply -f lemonade-demo.yaml

kubectl -n cert-demo get issuers
kubectl -n cert-demo get issuers.cert-manager.io letsencrypt-digitalocean -o json | jq ".status"
```

In the `demo-cert.yaml` file the issuer Ref name (`spec.issuerRef.name`) field should
be the same name as in the `lemonade-demo.yaml` file metadeata name field (`metadata.name`).  
See: https://docs.cert-manager.io/en/release-0.7/tasks/issuing-certificates/index.html
Also, the namespaces for the Issuer, the Issuer's api token secret and the Certificate must be the same.
```
kubectl apply -f demo-cert.yaml 
kubectl  -n cert-demo describe certificate tcbtech-corp-com-test-cert

kubectl -n cert-demo get certificaterequests.cert-manager.io 
kubectl -n cert-demo get certificaterequests.cert-manager.io tcbtech-corp-com-test-cert-7hshc
kubectl -n cert-demo get certificaterequests.cert-manager.io tcbtech-corp-com-test-cert-7hshc  -o json | jq -r '.spec.request' | base64 -d | openssl req -noout -text

kubectl -n cert-demo describe certificate tcbtech-corp-com-test-cert
```

Once the certificate is shown as `Ready` then the certificate found in
the secret for the cert and in the `certificaterequests.cert-manager.io` object.

``` 
kubectl -n cert-demo get secrets tcbtech-corp-com-test-cert -o json
kubectl -n cert-demo get secrets tcbtech-corp-com-test-cert -o json | jq -r '.data."tls.crt"' | base64 -d
kubectl -n cert-demo get secrets tcbtech-corp-com-test-cert -o json | jq -r '.data."tls.crt"' | base64 -d | openssl x509 -noout -text

kubectl -n cert-demo get certificaterequests.cert-manager.io tcbtech-corp-com-test-cert-sksqp  -o json | jq -r '.status.certificate' | base64 -d | openssl x509 -noout -text
```


*Note:* The reply from Let's Encrypt includes a certifcate chain with
multiple certificates. The easist way to break apart the chain is with
an editor and put them in separate files:

```
cat tcb-cert-chain-cert-1.txt | openssl x509 -noout -text
cat tcb-cert-chain-cert-2.txt | openssl x509 -noout -text
cat tcb-cert-chain-cert-3.txt | openssl x509 -noout -text
```

* Certifcate 1 is the certicate for tcbtech-corp
  * Issuer: `C=US, O=Let's Encrypt, CN=R3`, an intermediate certifcate from Let's Encrypt.
  * X509v3 Authority Key Identifier: `keyid:14:2E`
  * OCSP - URI:http://r3.o.lencr.org
  * CA Issuers - URI:http://r3.i.lencr.org/
* Certificate 2 is `C=US, O=Let's Encrypt, CN=R3`
  * Issuer: `C=US, O=Internet Security Research Group, CN=ISRG Root X1`.
  * X509v3 Subject Key Identifier: `14:2E`
  * X509v3 Authority Key Identifier: `keyid:79:B4`
* Certificate 3 is `C=US, O=Internet Security Research Group, CN=ISRG Root X1`
  * Issuer: `O=Digital Signature Trust Co., CN=DST Root CA X3`
  * X509v3 Subject Key Identifier: `79:B4`
  * X509v3 Authority Key Identifier: `keyid:C4:A7`
```
cat /etc/ssl/certs/DST_Root_CA_X3.pem  | openssl x509 -noout -text 
```
* Let's Encrypt root is `O = Digital Signature Trust Co., CN = DST Root CA X3`
  * Issuer: `O = Digital Signature Trust Co., CN = DST Root CA X3`
  * X509v3 Subject Key Identifier: `C4:A7`
  * X509v3 Authority Key Identifier: *not present*


## Subject Key Identifier:

```
cat tcb-cert-chain-cert-3.txt | openssl x509 -pubkey -noout > letsencrypt_X3_pub.pem
openssl rsa -in letsencrypt_X3_pub.pem -pubin -outform der | openssl dgst -sha160
```


# Java

```
keytool -list -v -keystore /Library/Java/JavaVirtualMachines/openjdk-14.0.2.jdk/Contents/Home/lib/security/cacerts > java-cert-list

keytool -export -keystore /Library/Java/JavaVirtualMachines/openjdk-14.0.2.jdk/Contents/Home/lib/security/cacerts  -alias "identrustdstx3 [jdk]"  -file identrustdstx3.p12
openssl x509 -inform der -in identrustdstx3.p12 -noout -text
 
```


# Troubleshooting

Is your api token correct?
```
kubectl -n cert-demo describe challenges.acme.cert-manager.io
```

# Notes on Cert-Manager:

`certificaterequests.cert-manager.io is not equal to CertificateSigningRequest in certificates.k8s.io/v1

# Documentation

https://cert-manager.io/docs/installation/

https://cert-manager.io/docs/configuration/acme/dns01/digitalocean/

https://cert-manager.io/docs/tutorials/acme/ingress/
See steps 6

https://cert-manager.io/docs/usage/certificate/

Trouble shooting acme certificates
https://cert-manager.io/docs/faq/acme/
