# Overview

Directory for research Local Trust Domains.

See: https://docs.google.com/document/d/1L3h5J5eNi9puMGBGiQCyYxGTR9obzrTPI3P-ixsKVxg  
And: https://gitlab.com/markphahn/practical-tls-advice  

Starting with building a website with a certificate, that is
then updated via Let's Encrypt ACME.

# Whats in this directory:

* cert-bot - (sketchy) information about installing cert-bot on a vm running nginx
* cert-manager - information about installing cert-manager on a cluster
* bolder - infomation about running `boulder` Let's Encrypt's implementation of an ACME-based CA


# demo machine
```
ssh -i ~/.ssh/tls-azure-user.pem azureuser@tls-test.westus2.cloudapp.azure.com
ssh -i ~/.ssh/tls-azure-user.pem azureuser@52.247.222.51

ssh -i ~/.ssh/tls-azure-user.pem azureuser@tls-boulder.westus2.cloudapp.azure.com

ssh -i ~/.ssh/tls-azure-user.pem azureuser@tls-cfssl.westus2.cloudapp.azure.com

nslookup -query=any tcbtech-corp.com
nslookup -query=any tls-test.tcbtech-corp.com
```
## Create test vritual machine

* Use the `tls-azure-user` ssh key stored in Azure.  Corresponds to the
`~/.ssh/tls-azure-user.pem` private key file

```
sudo apt update
sudo apt -y full-upgrade
```

# create a self-signed cert and copy to directory

```
sudo apt install golang-cfssl

echo '{"CN":"tls-test.tcbtech-corp.com","hosts":["tls-test.tcbtech-corp.com"],"key":{"algo":"ecdsa","size":256},"names":[{"C":"US","ST":"WA","L":"Issaquah"}]}' > csr
cfssl selfsign tls-test.tcbtech-corp.com csr | jq > cert-info.txt
jq -r < cert-info.txt '.cert' > tls-test.crt
jq -r < cert-info.txt '.csr' > tls-test.csr
jq -r < cert-info.txt '.key' > tls-test.key

sudo cp tls-test.crt /etc/ssl/certs/
sudo cp  tls-test.key /etc/ssl/private/

```

Setup the server site configuration:

```
sudo mkdir /var/tls-test
sudo mkdir /var/tls-test/html
sudo cp /var/www/html/index*.html /var/tls-test/html/index.html
```

Create a file called `/etc/nginx/sites-available/tls-test.tcbtech-corp.com`
with the following configuration

```
server {
	# SSL configuration
	listen 443 ssl default_server;
	ssl_certificate     /etc/ssl/certs/tls-test.crt;
	ssl_certificate_key /etc/ssl/private/tls-test.key;

	root /var/tls-test/html;

	server_name tls-test.tcbtech-corp.com;

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files $uri $uri/ =404;
	}
}
```

Then:
```
sudo ln -s /etc/nginx/sites-available/tls-test.tcbtech-corp.com /etc/nginx/sites-enabled
ls -ltra /etc/nginx/sites-enabled/

sudo rm /etc/nginx/sites-enabled/default

sudo systemctl restart nginx

curl -k https://localhost

openssl s_client -connect localhost:443

```

https://tls-test.tcbtech.net
https://nginx.tcbtech-corp.com


# Let's Encrypt on a VM
```
apt-get update
sudo apt-get install certbot
apt-get install python-certbot-nginx
```




