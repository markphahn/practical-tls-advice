# Overview

What follows is a collection of interesting background matetial we ran
across when researching this presentation. Do not consider this list
authoratative. We do not endorse any article or reference listed, in
fact we strongly diagree with a few. Read them, think about them and
draw your own conclusions.

There also little thought given to the order of these entries, either.

# Current and Prior work:

- https://tcbtech.com/practicaltls
- https://tcbtech.com/kubetls
- https://gitlab.com/markphahn/practical-tls-advice  
- https://gitlab.com/gauntletwizard_net/kubetls/  
- https://www.usenix.org/conference/srecon20americas/presentation/hahn
  - (https://www.youtube.com/watch?v=w-flctOnpsc)

# RFCs

- TLS 1.3 - [RFC 8446](https://datatracker.ietf.org/doc/html/rfc8446)
- TLS 1.2 - [RFC 5246](https://datatracker.ietf.org/doc/html/rfc5246)
- x509v3 - [RFC 5280](https://datatracker.ietf.org/doc/html/rfc5280)
  - https://en.wikipedia.org/wiki/X.509
- OCSP - [RFC 2560](https://datatracker.ietf.org/doc/html/rfc2560)
- Certificate Transparency - [RFC 6962](https://datatracker.ietf.org/doc/html/rfc6962)

# Wikipedia

- https://en.wikipedia.org/wiki/Transport_Layer_Security
- https://en.wikipedia.org/wiki/X.509
- https://en.wikipedia.org/wiki/ASN.1
- https://en.wikipedia.org/wiki/Automated_Certificate_Management_Environment
- https://en.wikipedia.org/wiki/Forward_secrecy
- https://en.wikipedia.org/wiki/Certificate_Transparency
- https://en.wikipedia.org/wiki/HTTPS
- https://en.wikipedia.org/wiki/GRPC
- https://en.wikipedia.org/wiki/Public_key_infrastructure

# Articles

- https://cabforum.org/
- https://www.chromium.org/Home/chromium-security/root-ca-policy
- https://wiki.mozilla.org/CA
- https://support.apple.com/en-bh/HT212140
- https://docs.microsoft.com/en-us/security/trusted-root/participants-list
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies
- https://www.itu.int/en/ITU-T/asn1/Pages/introduction.aspx
- https://aws.amazon.com/cloudhsm/
- https://azure.microsoft.com/en-us/services/azure-dedicated-hsm
- https://cloud.google.com/kms/docs/hsm
- https://istio.io/latest/docs/tasks/security/cert-management/
- https://github.com/cloudflare/cfssl
- https://learn.hashicorp.com/tutorials/vault/pki-engine#policy-requirements
- https://github.com/letsencrypt/boulder
- https://cert-manager.io/
- https://letsencrypt.org/certificates/
- https://pkg.go.dev/crypto/tls
- https://docs.python.org/3/library/ssl.html
- https://www.bouncycastle.org/
- https://go.dev/blog/tls-cipher-suites 
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
- https://docs.microsoft.com/en-us/dotnet/framework/network-programming/tls
- https://docs.oracle.com/en/java/javase/14/docs/api/java.base/javax/net/ssl/package-summary.html
- https://www.a10networks.com/blog/key-differences-between-tls-1-2-and-tls-1-3/
- https://www.ssl.com/blogs/how-do-browsers-handle-revoked-ssl-tls-certificates/
- https://www.imperialviolet.org/2011/03/18/revocation.html
- https://blog.mozilla.org/security/2020/01/09/crlite-part-1-all-web-pki-revocations-compressed/
- https://www.smacktls.com/
- https://www.ibm.com/docs/en/sdk-java-technology/7?topic=certificate-x509-certificates
- https://www.ateam-oracle.com/tls-and-java
- https://www.ateam-oracle.com/tls-saas-web-services-connections

# Other

- https://www.websequencediagrams.com/
- https://marp.app/
- http://www.plantuml.com
- https://app.diagrams.net/
